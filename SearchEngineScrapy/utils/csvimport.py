import csv
from io import open
from scrapy.conf import settings
from scrapy.exceptions import CloseSpider


def ImportFromCSV():
        csv_filename = settings.get('SEARCHES_FILE', None)

        if not csv_filename:
            raise CloseSpider("You must specificy SEARCHES_FILE='searches.csv' in settings.py")

        csv_fields = settings.getlist('CSV_IMPORT_FIELDS', ['query', 'engine', 'pages'])
        skip_header = settings.getbool('CSV_IMPORT_SKIP_HEADER', True)
        csv_delimiter = settings.get('CSV_DELIMITER', ',')
        csv_lineterminator = settings.get('CSV_LINETERMINATOR', '\n')

        with open(csv_filename, encoding='utf8') as csvfile:
            dr = csv.DictReader(csvfile, fieldnames=csv_fields, delimiter=csv_delimiter, lineterminator=csv_lineterminator)

            if skip_header:
                next(dr)

            for row in dr:
                row['query'] = row['query'].lower().strip()
                row['engine'] = row['engine'].lower().strip()
                row['pages'] = row['pages'].lower().strip()

                try:
                    row['pages'] = int(row['pages'])
                except:
                    row['pages'] = None

                if not row['engine']:
                    row['engine'] = 'bing'

                if not row['pages']:
                    row['pages'] = 3

                yield row
