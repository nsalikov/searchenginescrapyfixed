# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem
from scrapy.exceptions import CloseSpider


class FilterOutPipeline(object):

    def process_item(self, item, spider):

        if 'url' not in item:
            raise DropItem("Unable to fine 'url' filed: {}:".format(item))

        if item['url'] and item['url'].startswith('https://www.quora.com'):
            return item

        raise DropItem("Wrong url: {}:".format(item['url']))


class DuplicatesPipeline(object):

    def __init__(self):
        self.seen = set()


    def process_item(self, item, spider):

        return self.filter_by_key(item, spider, 'url')


    def filter_by_key(self, item, spider, key):
        if key not in item or not item[key]:
            raise DropItem("Can't not found {}:".format(key))
        if item[key] in self.seen:
            raise DropItem("Duplicate {}:".format(key))
        else:
            self.seen.add(item[key])
            return item
