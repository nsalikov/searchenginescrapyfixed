import scrapy

from scrapy.selector import Selector
from scrapy.spider import Spider

from SearchEngineScrapy.utils.searchengines import SearchEngineResultSelector
from SearchEngineScrapy.utils.searchenginepages import SearchEngineURLs
from SearchEngineScrapy.utils.csvimport import ImportFromCSV

class SearchEngineScrapy(Spider):
    name = "searchenginespider"

    allowed_domains = ['google.com', 'bing.com', 'ask.com', 'yahoo.com', 'yandex.com', 'baidu.com']


    def start_requests(self):
        for row in ImportFromCSV():

            urls = SearchEngineURLs(row['query'], row['engine'], row['pages'])
            selector = SearchEngineResultSelector[row['engine']]
            meta = {'selector': selector, 'query': row['query'], 'engine': row['engine']}

            for url in urls:
                yield scrapy.Request(url, meta=meta, callback=self.parse)


    def parse(self, response):
        selector = response.meta['selector']

        for url in response.css(selector).extract():
            d = {}

            d['query'] = response.meta['query']
            d['engine'] = response.meta['engine']
            d['url'] = url

            yield d
